const express = require('express'); // import (avec require()) du module express (dans la constante express)
// express est un framework web servant à construire des applications web
const fs = require('fs'); // on fait la même chose avec fs
// fs (file system) est un module servant à gérer et manipuler des fichiers
const app = express(); // app exécute express

//  app.get () <=> app va chercher (get) le fichier entré en argument : ici, data,
//  qui correspondond au menu du restaurant. L'application va ensuite effectuer l'arrow fonction entrée
//  en second argument
app.get('/data', (request, response) => {   //  request correspondra à la requête (qu'on enverra plus tard)
                                            //  et response ce qui sera effectué en réponse à celle-ci

    fs.readFile('data.json', (err, data) => { //    le module fs va lire le menu
        if (err){   // s'il y a une erreurr
            response.status(500).json( { // le code d'erreur 500 indique que la requête envoyée n'a pas pu 
                                        // être traitée pour une raison non-identifiée
                message: "Une erreur est survenue lors de la lecture des données du menu",
                error: err
            });
        } else {    // 200 est le code HTTP signifiant que l'opération est un succès
            response.status(200).json(JSON.parse(data));
        }                             
    });
});

app.listen(4000, () => { // app lie le port 4000 à l'hôte, afin d'effectuer la fonction ci-dessous
    console.log("L'application tourne sur le port 4000");
});
